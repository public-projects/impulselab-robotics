
"Wirtschaft trifft Wissenschaft"
3. Lausitz-Netzwerkveranstaltung
Chancen der Robotik der Zukunft

Am 14.11.2023 an der TU Dresden
Dülfersaal


Agenda:
9:30 Uhr
Anmeldung und Kaffee 

Moderation Vormittag: n.n.
10:00 Uhr
Grußworte und Rahmenvorträge
                                                                          15`
TU Dresden
                                                                          15`
BTU Cottbus
                                                                          30`
Vertreter:in Politik Sachsen
                                                                          30`
Vertreter:in Politik Brandenburg
11:30 Uhr
Mittagspause 
12:30 Uhr
Impulsvorträge
                                                                          25`
Impuls I: 
BTU Cottbus
                                                                          25`
Impuls II: 
TU Dresden 
                                                                          25`
Impuls III: 
Externe:r Expert:in
                                                                         15` 
Reflektion/Nachfragen
14:00 Uhr
Kaffeepause
14:15 Uhr
Podiumsdiskussion
 Vertreter:in Wirtschaft
 Vertreter:in Wissenschaft
 Vertreter:in Politik Sachsen
 Vertreter:in Politik Brandenburg
Moderation: n.n.
16:00 Uhr
Networking, Coffee Talk
17:00 Uhr 
Ende der Veranstaltung

